
public class ThreadHW extends Thread {
    public static void main(String[] args){
        MyThread myThread1 = new MyThread("Hello KSHRD!\n",300);
        MyThread myThread2 = new MyThread("**********************************\n",300);
        MyThread myThread3 = new MyThread("I will try my best to be here at HRD.\n",300);
        MyThread myThread4 = new MyThread("----------------------------------\n",300);
        MyThread myThread5 = new MyThread("Downloading..........",300);
        MyThread myThread6 = new MyThread("Completed 100%!",0);

        myThread1.start();
        try {
            myThread1.join();
        }catch (Exception e){}

        myThread2.start();
        try {
            myThread2.join();
        }catch (Exception e){}

        myThread3.start();
        try {
            myThread3.join();
        }catch (Exception e){}

        myThread4.start();
        try {
            myThread4.join();
        }catch (Exception e){}

        myThread5.start();
        try {
            myThread5.join();
        }catch (Exception e){}

        myThread6.start();
        try {
            myThread6.join();
        }catch (Exception e){}
    }

}
