public class MyThread extends Thread {

    String letter;
    int sleep;

    public MyThread(String letter, int sleep) {
        this.letter = letter;
        this.sleep = sleep;
    }

    public void run() {
        try {
            for (int i=0; i<letter.length() ; i++) {
                System.out.print(letter.charAt(i));
                Thread.sleep(sleep);
            }
        } catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
        }
    }
}
